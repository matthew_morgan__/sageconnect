# Sage 200 PHP Connector

## .env Endpoint URLs

GET:QUERY_LAST_UPDATED

Example Response:  
{
    "boms": "2019-06-11 09:57:29",
    "products": "2019-06-12 12:21:56",
    "customers": "2019-06-12 12:23:29",
    "orders": "2019-06-12 12:23:29",
    "productgroups": "2019-05-03 12:06:26",
    "customer_count": 3000
}

GET:QUERY_MISMATCHED_LINES

Example Response:  
{
    "success": true,
    "orders": [
        "156755",
        "396692",
        "396818",
        "35677942"
    ]
}

"orders" is an Array of the SOPOrderReturnID reference for orders in the CRM that contain no products but have order value > 0

POST:SYNC_DELETED_CUSTOMERS  
POST:SYNC_PRODUCT_GROUPS  
POST:REMOTE_RECEIVE_TRIGGER  

## .env Directory/Setup

REMOTE_ROOT_DIR

This is the root directory where the data type subfolders will live.

Inside this directory you should create the following folders.  

- works
- products
- orders
- orderlines
- despatchadd
- customers
- boms
