<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'ProductGroup';
    public $timestamps = false;
}
