<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\SageCustomer;
use DB;
use App\ProductGroup;
use App\SageOrder;

class doExport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
  
        $client = new Client(); //GuzzleHttp\Client
        $date_of_last_updates = $client->request('GET',env('QUERY_LAST_UPDATED'), [
               'verify' => false,
        ]);
        $update_dates = json_decode($date_of_last_updates->getBody());
        $last_boms_update = Carbon::createFromFormat('Y-m-d H:i:s', $update_dates->boms);
        $last_products_update = Carbon::createFromFormat('Y-m-d H:i:s', $update_dates->products);
        $last_customer_update = Carbon::createFromFormat('Y-m-d H:i:s', $update_dates->customers);
        $last_order_update = Carbon::createFromFormat('Y-m-d H:i:s', $update_dates->orders);
        $last_group_update = Carbon::createFromFormat('Y-m-d H:i:s', $update_dates->productgroups);

        app('App\Http\Controllers\ExportController')->customersExport($last_customer_update);
        app('App\Http\Controllers\ExportController')->ordersExport($last_order_update);
        //app('App\Http\Controllers\ExportController')->worksExport();
        app('App\Http\Controllers\ExportController')->productsExport($last_products_update);
        //app('App\Http\Controllers\ExportController')->compileBoms($last_boms_update);

        // export lines for orders with value but no lines synced to CRM
        $no_line_orders = $client->request('GET',env('QUERY_MISMATCHED_LINES'), [
               'verify' => false,
        ]);
        $orders_to_sync = json_decode($no_line_orders->getBody())->orders;
        $orders_to_sync = array_chunk($orders_to_sync, 1000);
        foreach ($orders_to_sync as $orders) {
            app('App\Http\Controllers\ExportController')->linesSync($orders);
            //Log::info("Exported lines for ".count(json_decode($no_line_orders->getBody())->orders)." mismatched orders.");
        }

        Log::info("Export Success");

        $client = new Client(); //GuzzleHttp\Client

        $group_updates = productgroup::where('DateTimeUpdated','>',$last_group_update)
                    ->select('ProductGroupID','Description','DateTimeUpdated')
                    ->get();
        if($group_updates->count() > 0){        
            $result = $client->request('POST',env('SYNC_PRODUCT_GROUPS'), [
                    'verify' => false,
                    'json' => ['groups' => $group_updates]
            ]);
        }

        $result = $client->post(env('REMOTE_RECEIVE_TRIGGER'), [
               'verify' => false,
               'json' => ['timestamp' => "New Data Available ".Carbon::now()->format('d-m-Y H:i:s')]
        ]);

        if(SageCustomer::select('SLCustomerAccountID')->pluck('SLCustomerAccountID')->count() !== $update_dates->customer_count){
        $result = $client->request('POST',env('SYNC_DELETED_CUSTOMERS'), [
                'verify' => false,
                'json' => ['customers' => SageCustomer::select('SLCustomerAccountID')->pluck('SLCustomerAccountID')->toArray()]
        ]);
        }


    }
}
