<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SageCustomer extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'SLCustomerAccount';
    public $timestamps = false;
    //protected $dates = ['DateTimeUpdated'];
    
    public function Currency()
    {
        return $this->hasOne('App\SageCurrency','SYSCurrencyID','SYSCurrencyID');
    }

    public function PrimaryAddress()
    {
        return $this->hasOne('App\SageCustomerAddress','SLCustomerAccountID','SLCustomerAccountID');
    }

    public function AdditionalInfo()
    {
        return $this->hasOne('App\SageAdditionalInfo','sage_reference','SLCustomerAccountID');
    }
}
