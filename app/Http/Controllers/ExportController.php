<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use DB;
use Log;
use Excel;
use App\Product;
use App\SageCustomer;
use App\SageOrder;
use App\WorksOrder;
use App\SageOrderLine;
use App\DespatchAddress;
use App\Jobs\doExport;

class ExportController extends Controller
{

    public function customersExport($last_customer_update, $sql = false)
    {
        ini_set('memory_limit','500M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'CustomerOutput_'.time();

        $fields = array(
            'SLCustomerAccount.SLCustomerAccountID',
            'SLCustomerAccount.CustomerAccountNumber',
            'SLCustomerAccount.CustomerAccountName',
            'SLCustomerAccount.AccountBalance',
            'SLCustomerAccount.CreditLimit',
            'SLCustomerAccount.SYSCurrencyID',
            'SLCustomerAccount.AccountIsOnHold',
            'SLCustomerAccount.DateAccountdetailsLastChanged',
            'SLCustomerAccount.DateOfLastTransaction',
            'SLCustomerAccount.AnalysisCode2',
            'SLCustomerAccount.AnalysisCode3',
            'SLCustomerLocation.AddressLine1',
            'SLCustomerLocation.AddressLine2',
            'SLCustomerLocation.AddressLine3',
            'SLCustomerLocation.AddressLine4',
            'SLCustomerLocation.PostCode',
            'SLCustomerAccount.CreditReference',
            'SLCustomerAccount.MainTelephoneAreaCode',
            'SLCustomerAccount.MainTelephoneCountryCode',
            'SLCustomerAccount.MainTelephoneSubscriberNumber',
            'SLCustomerAccount.MainFaxAreaCode',
            'SLCustomerAccount.MainFaxCountryCode',
            'SLCustomerAccount.MainFaxSubscriberNumber',
            'SLCustomerAccount.MainWebsite',
            'SLCustomerAccount.TaxRegistrationNumber',
            'SLCustomerAccount.DateTimeUpdated',
        );

        $sage_customer_updates = SageCustomer::join('SLCustomerLocation', 'SLCustomerAccount.SLCustomerAccountID', '=', 'SLCustomerLocation.SLCustomerAccountID');

            if($sql) {
                return $sage_customer_updates
                    ->select(['*'])
                    ->orderBy('SLCustomerAccount.DateAccountdetailsLastChanged')
                    ->take(10)
                    ->toSql();
            } else {
                $sage_customer_updates
                    ->select($fields)
                    ->where('SLCustomerAccount.DateTimeUpdated','>',$last_customer_update)
                    ->whereNotIn('SLCustomerAccount.AnalysisCode3',['Redditch','Euro Oils']) // need to make this generic
                    ->get();
            }

        if(count($sage_customer_updates) > 0){

        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/customers'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) use ($sage_customer_updates) {

            $excel->sheet('Customers', function($sheet) use ($sage_customer_updates) {
            $sheet->fromModel($sage_customer_updates);

            });

        })->store('csv', storage_path('exports/customers'), true);

        // Send Export File to Remote via FTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/customers/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/customers/'.$csvfile['file']);
        }

       Log::info($sage_customer_updates->count()." Customer Updates");

    }

    public function ordersExport($last_order_update)
    {
        $fields = array(
            'SOPOrderReturn.DocumentNo',
            'SOPOrderReturn.SOPOrderReturnID',
            'SOPOrderReturn.CustomerID',
            'SOPOrderReturn.DocumentStatusID',
            'SOPOrderReturn.CurrencyID',
            'SOPOrderReturn.TotalNetValue',
            'SOPOrderReturn.TotalGrossValue',
            'SOPOrderReturn.DocumentDate',
            'SOPOrderReturn.RequestedDeliveryDate',
            'SOPOrderReturn.PromisedDeliveryDate',
            'SOPOrderReturn.ReadyForInvoicePrint',
            'SOPOrderReturn.DocumentTypeID',
            'SOPOrderReturn.CustomerDocumentNo',
            'SOPOrderReturn.DateTimeUpdated',

            DB::connection('sage_sqlsrv')->raw('(SELECT count(*) from SOPOrderReturnLine WHERE SOPOrderReturnLine.SOPOrderReturnID=SOPOrderReturn.SOPOrderReturnID) as lines'),
        );

        $orders = SageOrder::join('SLCustomerAccount', 'SOPOrderReturn.CustomerID', '=', 'SLCustomerAccount.SLCustomerAccountID')
            ->select($fields)
            ->whereNotIn('SLCustomerAccount.AnalysisCode3',['Redditch','Euro Oils']) // need to make this generic
            ->where('SOPOrderReturn.DateTimeUpdated','>',$last_order_update)
            ->get();

        if($orders){
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/orders'));
        foreach ($orders->chunk(250) as $some_orders){
            if(count($some_orders) > 0){
            ini_set('memory_limit','500M');
            set_time_limit(3000);
            // Generate Filename
            $localFile = 'OrdersOutput_'.time();

            // Generate Export File
            $csvfile = Excel::create($localFile, function($excel) use ($some_orders) {

                $excel->sheet('Orders', function($sheet) use ($some_orders) {
                $sheet->fromModel($some_orders);

                });

            })->store('xlsx', storage_path('exports/orders'), true);

                    foreach ($some_orders->pluck('SOPOrderReturnID')->chunk(250) as $chunk){
                        $this->linesSync($chunk);
                        $this->despatchAddresses($chunk);
                    }

            // Send Export File to Remote via FTP
            Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/orders/'.$csvfile['file'],file_get_contents($csvfile['full']));
            Log::info($localFile." Uploaded");
            Storage::disk('base')->delete('exports/orders/'.$csvfile['file']);
            Log::info($localFile." Deleted");
            }
        }
    }

    }

    public function despatchAddresses($orders)
    {
        ini_set('memory_limit','500M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'DespatchAddressOutput_'.time();
        $addresses = DespatchAddress::whereIn('SOPOrderReturnID', $orders)->get();
        if($addresses && count($addresses) > 0){
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/despatchadd'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) use ($addresses) {

            $excel->sheet('DespatchAddresses', function($sheet) use ($addresses) {
            $sheet->fromModel($addresses);

            });

        })->store('xlsx', storage_path('exports/despatchadd'), true);

        // Send Export File to Remote via SFTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/despatchadd/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/despatchadd/'.$csvfile['file']);
        }
    }

    public function linesSync($orders)
    {
        ini_set('memory_limit','1200M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'OrderLinesOutput_'.time();
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/orderlines'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) use ($orders) {

            $excel->sheet('OrderLines', function($sheet) use ($orders) {

                $fields = array(
                    'SOPOrderReturnLine.SOPOrderReturnLineID',
                    'SOPOrderReturnLine.SOPOrderReturnID',
                    'SOPOrderReturnLine.LineTypeID',
                    'SOPOrderReturnLine.ItemCode',
                    'SOPOrderReturnLine.ItemDescription',
                    'SOPOrderReturnLine.LineQuantity',
                    'SOPOrderReturnLine.LineTotalValue',
                    'SOPOrderReturnLine.UnitSellingPrice',
                    'SOPOrderReturnLine.DespatchNoteComment',
                    'SOPOrderReturnLine.PickingListComment',
                    'SOPOrderReturnLine.ReadyForInvoicePrint',
                    'SOPOrderReturnLine.PostedInvoiceCreditQty'
                );

                $sheet->fromModel(SageOrderLine::select($fields)->join('SOPOrderReturn', 'SOPOrderReturn.SOPOrderReturnID', '=', 'SOPOrderReturnLine.SOPOrderReturnID')->whereIn('SOPOrderReturnLine.SOPOrderReturnID', $orders)->get());

            });

        })->store('xlsx', storage_path('exports/orderlines'), true);

        // Send Export File to Remote via SFTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/orderlines/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/orderlines/'.$csvfile['file']);
    }

    public function worksExport()
    {
        ini_set('memory_limit','500M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'WorksOutput_'.time();
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/works'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) {

            $excel->sheet('Works', function($sheet) {

                $fields = array(
                    'WorksOrderNumber',
                    'BomReference',
                    'BomDescription',
                    'DateEntered',
                    'DueDate',
                    'Status',
                    'LastCompletedDate',
                    'Warehouse',
                    'QtyRequired',
                    'QtyFinished',
                    'StockUnitName',
                );

                $sheet->fromModel(WorksOrder::select($fields)->where('DateEntered','>',Carbon::now()->endOfDay()->subMonths(1))->get());

            });

        })->store('csv', storage_path('exports/works'), true);

        // Send Export File to Remote via SFTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/works/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/works/'.$csvfile['file']);

    }

    public function productsExport($last_product_update)
    {
        ini_set('memory_limit','500M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'ProductsOutput_'.time();

        $exclude = [
            //107859, // Special Instructions
            //8120, // Packaging
            //10977742, // Empty Packaging
        ];

                        $fields = array(
                    'ItemID',
                    'Code',
                    'Name',
                    'ProductGroupID',
                    'Description',
                    'StockUnitName',
                    'DateTimeUpdated'
                );

                $products = Product::select($fields)->whereNotIn('ProductGroupID', $exclude)->where('DateTimeUpdated','>',$last_product_update)->get(); // makes assumption that bom changes update product

                $products = $products->map(function ($product) {
                    $product['fluid'] = null;
                    //$product['bomlinecount'] = 0;
                    if(!$product->Code || $product->Code == ''){} else {
                                $bom = DB::connection('sage_sqlsrv')->table('BomRecord')->select('BomRecordID')->where('Reference','=',$product->Code)->first();
                                if($bom){
                                    $bompackage = DB::connection('sage_sqlsrv')->table('BomBuildPackage')->select('BomBuildPackageID')->where('BomRecordID','=',$bom->BomRecordID)->first();
                                    $parts = DB::connection('sage_sqlsrv')->table('BomComponentLine')->where('BomBuildPackageID','=',$bompackage->BomBuildPackageID)->get();
                                    foreach($parts as $part){
                                        if(substr($part->StockCode, 0, 1) == 4){
                                            $product['fluid'] = $part->StockCode;
                                        }
                                    }
                                    //$product['bomlinecount'] = count($parts);
                                }
                            }
                                return $product;
                            });
        if($products && count($products) > 0){
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/products'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) use ($products) {

            $excel->sheet('Products', function($sheet) use ($products) {
            $sheet->fromModel($products);

            });

        })->store('csv', storage_path('exports/products'), true);

        // Send Export File to Remote via SFTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/products/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/products/'.$csvfile['file']);
    }

    }


    public function compileBoms($last_boms_update){

            $products = Product::select('Code')->get();

            foreach($products as $product){
                if(!$product->Code || $product->Code == ''){} else {
                                $bom = DB::connection('sage_sqlsrv')->table('BomRecord')->select('BomRecordID')->where('Reference','=',$product->Code)->first();
                                if($bom){
                                    $bompackage = DB::connection('sage_sqlsrv')->table('BomBuildPackage')->select('BomBuildPackageID')->where('BomRecordID','=',$bom->BomRecordID)->first();
                                    $parts = DB::connection('sage_sqlsrv')->table('BomComponentLine')->where('BomBuildPackageID','=',$bompackage->BomBuildPackageID)->get();
                                    foreach($parts as $part){
                                        $part_updated = Carbon::createFromFormat('Y-m-d H:i:s.u', $part->DateTimeUpdated);
                                        if($part_updated > $last_boms_update){
                                            if($part->BomComponentLineTypeID == 2){
                                                // this is where we would get/add the bom line breakdown for the fluid should we need it
                                                $fluid_bom = DB::connection('sage_sqlsrv')->table('BomRecord')->select('BomRecordID')->where('Reference','=',$part->StockCode)->first();
                                                if($fluid_bom){
                                                    $fluid_bompackage = DB::connection('sage_sqlsrv')->table('BomBuildPackage')->select('BomBuildPackageID')->where('BomRecordID','=',$fluid_bom->BomRecordID)->first();
                                                    $fluid_ingredients = DB::connection('sage_sqlsrv')->table('BomComponentLine')->where('BomBuildPackageID','=',$fluid_bompackage->BomBuildPackageID)->get();
                                                    foreach($fluid_ingredients as $ingredient){
                                                        $ingredient_updated = Carbon::createFromFormat('Y-m-d H:i:s.u', $ingredient->DateTimeUpdated);
                                                        if($ingredient_updated > $last_boms_update){
                                                           $bom_lines[] = [$ingredient->StockCode,$ingredient->Description,$ingredient->Quantity,$ingredient->UnitOfMeasure,$product->Code,$ingredient->DateTimeUpdated];
                                                        }
                                                    }
                                                }
                                            }
                                            $bom_lines[] = [$part->StockCode,$part->Description,$part->Quantity,$part->UnitOfMeasure,$product->Code,$part->DateTimeUpdated];
                                        }
                                    }
                                }
                            }
            }


        if(isset($bom_lines)){

        ini_set('memory_limit','500M');
        set_time_limit(3000);
        // Generate Filename
        $localFile = 'BomsOutput_'.time();
        $file = new Filesystem;
        $file->cleanDirectory(storage_path('exports/boms'));
        // Generate Export File
        $csvfile = Excel::create($localFile, function($excel) use ($bom_lines) {

            $excel->sheet('Boms', function($sheet) use ($bom_lines) {

                $sheet->fromArray($bom_lines,null, 'A1', true, false);

            });

        })->store('csv', storage_path('exports/boms'), true);

        // Send Export File to Remote via SFTP
        Storage::disk('ftp')->put(env('REMOTE_ROOT_DIR').'/boms/'.$csvfile['file'],file_get_contents($csvfile['full']));
        Storage::disk('base')->delete('exports/boms/'.$csvfile['file']);
        }
    }

    public function runExport(){
        $first = Carbon::createFromTime(0, 0, 0);
        $second = Carbon::createFromTime(23, 59, 59);
        if(Carbon::now()->between($first, $second))
        { // only sync during opening hours

        // Queue Export Functions
        $this->dispatch(new doExport());

        return "Export Complete!";

       } else {
        return "System sleeping...try again later!";
       }
   }

}
