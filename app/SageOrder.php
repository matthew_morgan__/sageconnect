<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SageOrder extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'SOPOrderReturn';
    public $timestamps = false;
    protected $dates = array('PromisedDeliveryDate','RequestedDeliveryDate','DocumentDate');

    public function OrderLines()
    {
        return $this->hasMany('App\SageOrderLine','SOPOrderReturnID','SOPOrderReturnID');
    }

    public function OrderStatus()
    {
        return $this->hasOne('App\SageOrderStatus','DocumentStatusID','DocumentStatusID');
    }

    public function OrderType()
    {
        return $this->hasOne('App\SageOrderType','SOPOrderReturnTypeID','DocumentTypeID');
    }

    public function Currency()
    {
        return $this->hasOne('App\SageCurrency','SYSCurrencyID','CurrencyID');
    }

    public function OrderCustomer()
    {
        return $this->belongsTo('App\SageCustomer','CustomerID','SLCustomerAccountID');
    }

    public function Address()
    {
        return $this->hasOne('App\SageCustomerDocDelivery','SOPOrderReturnID','SOPOrderReturnID');
    }

    public function AsInvoicedAddress()
    {
        return $this->hasOne('App\SageCustomerAddress','SLCustomerAccountID','CustomerID');
    }

    public function despatchAddress(){
        return $this->hasOne('App\DespatchAddress','SOPOrderReturnID','SOPOrderReturnID');
    }

    public function firstTimeOrder($customer,$productCode,$thisorder){
        $first = true;

            // TODO: if productCode has KX prefix check if ever been ordered by anyone else check per orer per customer

            $orders = SageOrder::where('CustomerID',$customer)->get();
            foreach ($orders as $order => $value) {
                if($thisorder == $value->DocumentNo){ continue; }
                $previousOrders = SageOrderLine::where('SOPOrderReturnID',$value->SOPOrderReturnID)->where('ItemCode',$productCode)->where('LineTypeID','0')->count();
                if($previousOrders > 0){ $first = false; break; }
            }

        // return true if first time
        return $first;
    }

    public function scopeWarehouse($query, $division)
    {
        // if user should see all customers of both divisions
        if($division == 'all'){
            return $query;
        }

        // if user should only see orders from specific division
        if($division !== 'all'){
            switch($division){
                case 'chorley':
                return $query->where('WarehouseID', '=', 8372);
                break;
                case 'redditch':
                return $query->where('WarehouseID', '=', 10627995);
                break;
                case 'all':
                return $query;
                break;
            }
        }

    }
}
