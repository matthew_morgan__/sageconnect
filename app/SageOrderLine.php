<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SageOrderLine extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'SOPOrderReturnLine';
    public $timestamps = false;
}