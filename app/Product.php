<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'StockItem';
    protected $connection = 'sage_sqlsrv';
    public $timestamps = false;

    
}