<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WorksOrder extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'WorksOrders';
    public $timestamps = false;
    protected $dates = array('DateEntered','DueDate','LastCompletedDate');
}
