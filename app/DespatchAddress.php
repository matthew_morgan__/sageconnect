<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DespatchAddress extends Model
{
    protected $connection = 'sage_sqlsrv';
    protected $table = 'SOPDocDelAddress';
    public $timestamps = false;
}
