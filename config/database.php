<?php 

return [

'default' => 'sage_sqlsrv',

'connections' => [

    'sage_sqlsrv' => [
            'driver'   => 'sqlsrv',
            'host'     => env('DB_SAGE_HOST', 'localhost'),
            'database' => env('DB_SAGE_DATABASE', 'forge'),
            'username' => env('DB_SAGE_USERNAME', 'forge'),
            'password' => env('DB_SAGE_PASSWORD', ''),
            'charset'  => 'utf8',
            'prefix'   => '',
        ],

]

];