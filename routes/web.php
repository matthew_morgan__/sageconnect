<?php

$router->get('/', function () use ($router) {
    return "Sage Connect App: ".$router->app->version();
});

$router->get('export', 'ExportController@runExport');

$router->get('customers/{sql}', 'ExportController@customersExport');
// $router->get('orders', 'ExportController@ordersExport');
// $router->get('lines', 'ExportController@orderLinesExport');
// $router->get('works', 'ExportController@worksExport');
// $router->get('products', 'ExportController@productsExport');
